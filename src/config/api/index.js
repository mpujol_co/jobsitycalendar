import { create } from "apisauce";

// define the api
const api = create({
  baseURL: process.env.REACT_APP_BASE_URL
});

export default api;
