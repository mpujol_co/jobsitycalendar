import React from "react";
import { string, oneOfType, arrayOf, node, bool, func } from "prop-types";
import Modal from "react-modal";
import { CUSTOM_STYLES } from "./constants";
import styles from "./styles.module.scss";

Modal.setAppElement("#root");

function ModalContainer({ isOpen, title, children, onClose }) {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onClose}
      style={CUSTOM_STYLES}
      contentLabel={title}
    >
      <div className={styles.titleContainer}>
        <div className={styles.title}>
          <h2>{title}</h2>
        </div>
        <button onClick={onClose}>Close</button>
      </div>
      {children}
    </Modal>
  );
}

ModalContainer.propTypes = {
  isOpen: bool,
  title: string,
  onClose: func,
  children: oneOfType([arrayOf(node), node]),
};

export default ModalContainer;
