import React from "react";
import { string, object } from "prop-types";
import TextField from "@material-ui/core/TextField";
import Layout from "../Layout";

function CustomDateTimePicker({ label, meta, input }) {
  return (
    <Layout label={label} meta={meta}>
      <TextField
        {...input}
        type="datetime-local"
        InputLabelProps={{
          shrink: true,
        }}
      />
    </Layout>
  );
}

CustomDateTimePicker.propTypes = {
  label: string,
  meta: object,
  input: object,
};

export default CustomDateTimePicker;
