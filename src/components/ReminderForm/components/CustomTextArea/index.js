import React from "react";
import { string, object } from "prop-types";
import Layout from "../Layout";
import styles from "./styles.module.scss";

function CustomTextArea({ label, meta, input }) {
  return (
    <Layout label={label} meta={meta}>
      <textarea {...input} className={styles.customTextArea} />
    </Layout>
  );
}

CustomTextArea.propTypes = {
  label: string,
  meta: object,
  input: object,
};

export default CustomTextArea;
