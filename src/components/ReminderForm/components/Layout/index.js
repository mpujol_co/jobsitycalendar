import React from "react";
import { string, oneOfType, arrayOf, node, object } from "prop-types";
import styles from "./styles.module.scss";

function Layout({ label, meta, children }) {
  return (
    <div className={styles.container}>
      {label && <label>{label}</label>}
      {meta.touched && !meta.active && meta.error && (
        <small className={styles.error}>{meta.error}</small>
      )}
      {children}
    </div>
  );
}

Layout.propTypes = {
  label: string,
  meta: object,
  children: oneOfType([arrayOf(node), node]),
};

export default Layout;
