import React, { useState } from "react";
import { string, object } from "prop-types";
import ColorPicker from "material-ui-color-picker";
import Layout from "../Layout";

function CustomColorPicker({ label, meta, input }) {
  const [bgColor, setBgColor] = useState(input.value || "#2c73b7");

  const handleChange = (color) => {
    setBgColor(color);
    input.onChange(color);
  };

  return (
    <Layout label={label} meta={meta}>
      <ColorPicker
        {...input}
        onChange={handleChange}
        style={{ backgroundColor: bgColor }}
      />
    </Layout>
  );
}

CustomColorPicker.propTypes = {
  label: string,
  meta: object,
  input: object,
};

export default CustomColorPicker;
