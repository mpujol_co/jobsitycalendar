import VALIDATION from "constants/formValidations";

const MAX_TEXT_LENGTH = 30;

export const FORM_FIELDS = {
  TEXT_REMINDER: "text",
  DATE_REMINDER: "date",
  CITY_REMINDER: "city",
  COLOR_REMINDER: "color",
};

export const VALIDATIONS = {
  [FORM_FIELDS.TEXT_REMINDER]: [
    VALIDATION.required,
    VALIDATION.maxLength(MAX_TEXT_LENGTH),
  ],
  [FORM_FIELDS.DATE_REMINDER]: [VALIDATION.required],
  [FORM_FIELDS.CITY_REMINDER]: [
    VALIDATION.required,
    VALIDATION.maxLength(MAX_TEXT_LENGTH),
  ],
};
