import React from "react";
import { validateForm } from "redux-form-validators";
import { func, bool, string, object } from "prop-types";
import ReminderForm from "./layout";
import { VALIDATIONS } from "./constants";

function ReminderFormContainer({
  formName,
  onSubmit,
  loading,
  successMessage,
  initialValues,
}) {
  return (
    <ReminderForm
      form={formName}
      onSubmit={onSubmit}
      loading={loading}
      validate={validateForm(VALIDATIONS)}
      initialValues={initialValues}
      successMessage={successMessage}
    />
  );
}

ReminderFormContainer.propTypes = {
  onSubmit: func,
  loading: bool,
  successMessage: string,
  formName: string,
  initialValues: object,
};

export default ReminderFormContainer;
