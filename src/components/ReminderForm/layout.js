import React from "react";
import { Field, reduxForm } from "redux-form";
import { func, bool, string } from "prop-types";
import CustomDateTimePicker from "./components/CustomDateTimePicker";
import CustomTextArea from "./components/CustomTextArea";
import CustomColorPicker from "./components/CustomColorPicker";
import { FORM_FIELDS, VALIDATIONS } from "./constants";
import styles from "./styles.module.scss";

function ReminderForm({ handleSubmit, error, loading, successMessage }) {
  return (
    <form onSubmit={handleSubmit} encType="multipart/form-data">
      <Field
        name={FORM_FIELDS.COLOR_REMINDER}
        component={CustomColorPicker}
        label="Color"
      />
      <Field
        name={FORM_FIELDS.DATE_REMINDER}
        component={CustomDateTimePicker}
        label="Reminder date and time"
        validate={VALIDATIONS[FORM_FIELDS.DATE_REMINDER]}
      />
      <Field
        name={FORM_FIELDS.CITY_REMINDER}
        component={CustomTextArea}
        label="City"
        validate={VALIDATIONS[FORM_FIELDS.CITY_REMINDER]}
      />
      <Field
        name={FORM_FIELDS.TEXT_REMINDER}
        component={CustomTextArea}
        label="Reminder text"
        validate={VALIDATIONS[FORM_FIELDS.TEXT_REMINDER]}
      />
      <div>
        <div className={styles.footer}>
          <button type="submit">{loading ? "Loading" : "Submit"}</button>
          <div>
            {error && <small className={styles.error}>{error}</small>}
            {!error && successMessage && (
              <small className={styles.success}>{successMessage}</small>
            )}
          </div>
        </div>
      </div>
    </form>
  );
}

ReminderForm.propTypes = {
  handleSubmit: func,
  loading: bool,
  error: string,
  successMessage: string,
};

export default reduxForm()(ReminderForm);
