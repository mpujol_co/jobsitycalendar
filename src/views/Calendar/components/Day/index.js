import React from "react";
import { string, bool, func } from "prop-types";
import { TARGET } from "redux/calendar/constants";
import styles from "../../styles.module.scss";

function Day({ day, disabled, isWeekend, daySelected, setCurrentDate }) {
  const CLASSES = `${disabled ? styles.dayDisabled : styles.day} ${
    isWeekend ? styles.isWeekend : ""
  } ${daySelected ? styles.daySelected : ""}`;

  const handleClick = () =>
    !disabled && !daySelected && setCurrentDate(TARGET.day, day);

  return (
    <div className={CLASSES} onClick={handleClick}>
      <p>{day}</p>
    </div>
  );
}

Day.propTypes = {
  day: string,
  disabled: bool,
  isWeekend: bool,
  daySelected: bool,
  setCurrentDate: func,
};

export default Day;
