import React from "react";
import { string, func } from "prop-types";
import { TARGET } from "redux/calendar/constants";
import {
  getMonthName,
  getPrevMonthAndYear,
  getNextMonthAndYear,
} from "utils/date";
import styles from "../../styles.module.scss";

function Header({ currentYear, currentMonth, setCurrentDate }) {
  const updateDate = (newDate) => {
    if (newDate.year !== currentYear) {
      setCurrentDate(TARGET.year, newDate.year);
    }
    setCurrentDate(TARGET.month, newDate.month);
  };

  const handlePrevMonthClick = () =>
    updateDate(getPrevMonthAndYear(currentMonth, currentYear));

  const handleNextMonthClick = () =>
    updateDate(getNextMonthAndYear(currentMonth, currentYear));

  return (
    <div className={styles.titleContainer}>
      <button onClick={handlePrevMonthClick}>Prev month</button>
      <div className={styles.title}>
        <h2>{`${getMonthName(currentMonth)}, ${currentYear}`}</h2>
      </div>
      <button onClick={handleNextMonthClick}>Next month</button>
    </div>
  );
}

Header.propTypes = {
  currentYear: string,
  currentMonth: string,
  setCurrentDate: func,
};

export default Header;
