import moment from "moment";
import { daysInMonth } from "utils/date";

export const setMonthDays = (year, month, weekdays) => {
  const TOTAL_DAYS = daysInMonth(year, month);
  const FIRST_DAY = moment(`${year}-${month}-01`, "YYYY-MM-DD");
  const LAST_DAY = moment(`${year}-${month}-${TOTAL_DAYS}`, "YYYY-MM-DD");
  const FIRST_DAY_INDEX = weekdays.findIndex(
    (name) => name === FIRST_DAY.format("dddd")
  );
  const LAST_DAY_INDEX = weekdays.findIndex(
    (name) => name === LAST_DAY.format("dddd")
  );

  const result = [];

  if (FIRST_DAY_INDEX !== 0) {
    let NEW_DAY = FIRST_DAY.subtract(1, "days");
    for (let j = FIRST_DAY_INDEX; j > 0; j--) {
      result.unshift(NEW_DAY.format("YYYY-MM-DD"));
      NEW_DAY = NEW_DAY.subtract(1, "days");
    }
  }

  for (let i = 1; i <= TOTAL_DAYS; i++) {
    result.push(
      moment(`${year}-${month}-${i}`, "YYYY-MM-D").format("YYYY-MM-DD")
    );
  }

  if (LAST_DAY_INDEX !== weekdays.length - 1) {
    let NEW_DAY = LAST_DAY.add(1, "days");
    for (let j = LAST_DAY_INDEX; j < weekdays.length - 1; j++) {
      result.push(NEW_DAY.format("YYYY-MM-DD"));
      NEW_DAY = NEW_DAY.add(1, "days");
    }
  }

  return result;
};
