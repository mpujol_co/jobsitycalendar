import React from "react";
import { connect } from "react-redux";
import { arrayOf, string, func } from "prop-types";
import { actionCreators as calendarActions } from "redux/calendar/actions";
import { TARGET } from "redux/calendar/constants";
import Header from "./components/Header";
import Day from "./components/Day";
import { getDay, getMonth, isWeekend, isDaySelected } from "utils/date";
import styles from "./styles.module.scss";
import { setMonthDays } from "./utils";

function Calendar({
  weekdays,
  currentYear,
  currentMonth,
  currentDay,
  setCurrentDate,
}) {
  const setHeader = () =>
    weekdays.map((day) => (
      <div key={day} className={styles.header}>
        {day}
      </div>
    ));

  const setBody = () =>
    setMonthDays(currentYear, currentMonth, weekdays).map((day) => (
      <Day
        key={day}
        day={getDay(day)}
        isWeekend={isWeekend(day)}
        disabled={currentMonth !== getMonth(day)}
        setCurrentDate={setCurrentDate}
        daySelected={isDaySelected(
          day,
          `${currentYear}-${currentMonth}-${currentDay}`
        )}
      />
    ));

  return (
    <>
      <Header
        currentMonth={currentMonth}
        currentYear={currentYear}
        setCurrentDate={setCurrentDate}
      />
      <div className={styles.calendarContainer}>
        {setHeader()}
        {setBody()}
      </div>
    </>
  );
}

Calendar.propTypes = {
  weekdays: arrayOf(string),
  currentYear: string,
  currentMonth: string,
  currentDay: string,
  setCurrentDate: func,
};

const mapStateToProps = (state) => ({
  weekdays: state.calendar[TARGET.weekdays],
  currentYear: state.calendar[TARGET.year],
  currentMonth: state.calendar[TARGET.month],
  currentDay: state.calendar[TARGET.day],
});

const mapDispatchToProps = (dispatch) => ({
  setCurrentDate: (target, value) =>
    dispatch(calendarActions.update(target, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
