import React from "react";
import { connect } from "react-redux";
import { string, shape, arrayOf } from "prop-types";
import { TARGET as CALENDAR_TARGET } from "redux/calendar/constants";
import { TARGET as REMINDER_TARGET } from "redux/reminder/constants";
import Header from "./components/Header";
import List from "./components/List";
import { isDaySelected } from "utils/date";

function Reminder({ currentDay, currentMonth, currentYear, reminders }) {
  const currentReminders = reminders.filter((value) =>
    isDaySelected(value.date, `${currentYear}-${currentMonth}-${currentDay}`)
  );

  return (
    <>
      <Header hasReminders={!!currentReminders.length} />
      <div>
        {currentReminders.length ? (
          <List reminders={currentReminders} />
        ) : (
          "No reminders found."
        )}
      </div>
    </>
  );
}

Reminder.propTypes = {
  currentYear: string,
  currentMonth: string,
  currentDay: string,
  reminders: arrayOf(
    shape({
      date: string,
      text: string,
    })
  ),
};

const mapStateToProps = (state) => ({
  currentYear: state.calendar[CALENDAR_TARGET.year],
  currentMonth: state.calendar[CALENDAR_TARGET.month],
  currentDay: state.calendar[CALENDAR_TARGET.day],
  reminders: state.reminder[REMINDER_TARGET.list],
});

export default connect(mapStateToProps)(Reminder);
