import React, { useState } from "react";
import { connect } from "react-redux";
import { arrayOf, shape, string, func, bool } from "prop-types";
import { actionCreators as reminderActions } from "redux/reminder/actions";
import { TARGET } from "redux/reminder/constants";
import Modal from "components/Modal";
import ReminderForm from "components/ReminderForm";
import styles from "../../styles.module.scss";
import { FORM_NAME } from "./constants";

function UpdateReminder({ reminders, updateReminder, loading, item }) {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);
  const [itemId, setItemId] = useState(null);

  const handleUpdate = (id) => () => {
    setItemId(id);
    setIsOpenModal(true);
  };

  const handleCloseModal = () => {
    setIsOpenModal(false);
    setSuccessMessage(null);
  };

  const handleSubmit = (values) => {
    updateReminder(itemId, values);
    setSuccessMessage("Reminder updated successfully.");
  };

  const initialValues = () => reminders.find((item) => item.id === itemId);

  return (
    <>
      <button onClick={handleUpdate(item.id)} className={styles.update}>
        Update
      </button>
      {isOpenModal && (
        <Modal
          isOpen={isOpenModal}
          onClose={handleCloseModal}
          title="Update reminder"
        >
          <ReminderForm
            formName={FORM_NAME}
            onSubmit={handleSubmit}
            loading={loading}
            successMessage={successMessage}
            initialValues={initialValues()}
          />
        </Modal>
      )}
    </>
  );
}

UpdateReminder.propTypes = {
  reminders: arrayOf(
    shape({
      date: string,
      text: string,
    })
  ),
  updateReminder: func,
  loading: bool,
  item: shape({
    id: string,
  }),
};

const mapStateToProps = (state) => ({
  loading: state.reminder[`${TARGET.list}Loading`],
});

const mapDispatchToProps = (dispatch) => ({
  updateReminder: (id, values) => dispatch(reminderActions.update(id, values)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdateReminder);
