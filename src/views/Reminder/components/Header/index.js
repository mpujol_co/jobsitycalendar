import React, { useState } from "react";
import { bool, func, string } from "prop-types";
import { connect } from "react-redux";
import { actionCreators as reminderActions } from "redux/reminder/actions";
import { TARGET as CALENDAR_TARGET } from "redux/calendar/constants";
import { TARGET as REMINDER_TARGET } from "redux/reminder/constants";
import Modal from "components/Modal";
import ReminderForm from "components/ReminderForm";
import { FORM_FIELDS } from "components/ReminderForm/constants";
import { dateTimeLocal } from "utils/date";
import styles from "../../styles.module.scss";
import { FORM_NAME } from "./constants";

function Header({
  addReminder,
  loading,
  currentYear,
  currentMonth,
  currentDay,
  hasReminders,
  deleteAllReminders,
}) {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);

  const handleAddReminder = () => setIsOpenModal(true);

  const handleDeleteAllReminders = () => deleteAllReminders();

  const handleCloseModal = () => {
    setIsOpenModal(false);
    setSuccessMessage(null);
  };

  const handleSubmit = (values) => {
    addReminder(values);
    setSuccessMessage("Reminder added successfully.");
  };

  const initialValues = () => ({
    [FORM_FIELDS.DATE_REMINDER]: dateTimeLocal(
      `${currentYear}-${currentMonth}-${currentDay}`
    ),
  });

  return (
    <div className={styles.titleContainer}>
      <div className={styles.title}>
        <h2>Reminders</h2>
      </div>
      <div className={styles.buttonContainer}>
        {hasReminders && (
          <button onClick={handleDeleteAllReminders} className={styles.delete}>
            Delete all reminders
          </button>
        )}
        <button onClick={handleAddReminder}>Add reminder</button>
      </div>

      {isOpenModal && (
        <Modal
          isOpen={isOpenModal}
          onClose={handleCloseModal}
          title="Add reminder"
        >
          <ReminderForm
            formName={FORM_NAME}
            onSubmit={handleSubmit}
            loading={loading}
            successMessage={successMessage}
            initialValues={initialValues()}
          />
        </Modal>
      )}
    </div>
  );
}

Header.propTypes = {
  loading: bool,
  addReminder: func,
  currentYear: string,
  currentMonth: string,
  currentDay: string,
  hasReminders: bool,
  deleteAllReminders: func,
};

const mapStateToProps = (state) => ({
  loading: state.reminder[`${REMINDER_TARGET.list}Loading`],
  currentYear: state.calendar[CALENDAR_TARGET.year],
  currentMonth: state.calendar[CALENDAR_TARGET.month],
  currentDay: state.calendar[CALENDAR_TARGET.day],
});

const mapDispatchToProps = (dispatch) => ({
  addReminder: (value) => dispatch(reminderActions.add(value, FORM_NAME)),
  deleteAllReminders: () => dispatch(reminderActions.deleteAllReminders()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
