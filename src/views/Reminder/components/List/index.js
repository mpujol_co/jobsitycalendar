import React, { Fragment } from "react";
import { connect } from "react-redux";
import { arrayOf, shape, string, func } from "prop-types";
import { actionCreators as reminderActions } from "redux/reminder/actions";
import { getTime, currentTimestamp } from "utils/date";
import City from "../City";
import UpdateReminder from "../UpdateReminder";
import styles from "../../styles.module.scss";

function List({ reminders, deleteReminder }) {
  const sortReminders = (item1, item2) =>
    currentTimestamp(item1.date) - currentTimestamp(item2.date);

  const handleDelete = (id) => () => deleteReminder(id);

  return (
    <div className={styles.listContainer}>
      {reminders.sort(sortReminders).map((item) => (
        <Fragment key={item.id}>
          <div style={{ backgroundColor: item.color }}>
            <p>{getTime(item.date)}</p>
          </div>
          <City item={item} />
          <div>
            <p>{item.text}</p>
          </div>
          <button onClick={handleDelete(item.id)} className={styles.delete}>
            Delete
          </button>
          <UpdateReminder reminders={reminders} item={item} />
        </Fragment>
      ))}
    </div>
  );
}

List.propTypes = {
  reminders: arrayOf(
    shape({
      date: string,
      text: string,
    })
  ),
  deleteReminder: func,
};

const mapDispatchToProps = (dispatch) => ({
  deleteReminder: (id) => dispatch(reminderActions.delete(id)),
});

export default connect(null, mapDispatchToProps)(List);
