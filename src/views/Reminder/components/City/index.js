import React, { useState } from "react";
import { shape, string, bool, func, object } from "prop-types";
import { connect } from "react-redux";
import { actionCreators as reminderActions } from "redux/reminder/actions";
import { TARGET } from "redux/reminder/constants";
import Modal from "components/Modal";
import { getDate } from "utils/date";
import styles from "./styles.module.scss";

function City({ item, loading, deleteWeatherInfo, getWeather, data }) {
  const [isOpenModal, setIsOpenModal] = useState(false);

  const WEATHER = data?.weather?.[0];
  const ERROR = data?.error?.[0]?.msg;

  const handleWeather = () => {
    getWeather(item.city, item.date);
    setIsOpenModal(true);
  };

  const handleCloseModal = () => {
    setIsOpenModal(false);
    deleteWeatherInfo();
  };

  return (
    <>
      <div>
        <p>{item.city}</p>
      </div>
      <button onClick={handleWeather}>Weather</button>
      {isOpenModal && (
        <Modal
          isOpen={isOpenModal}
          onClose={handleCloseModal}
          title={`Weather in ${data?.request?.[0]?.query || item.city}`}
        >
          <h3>{`Date: ${getDate(item.date)}`}</h3>
          {loading ? (
            "Loading"
          ) : WEATHER ? (
            <div className={styles.container}>
              <div>
                <p>Temp in C</p>
              </div>
              <div>
                <p>{WEATHER.avgtempC}</p>
              </div>
              <div>
                <p>Temp in F</p>
              </div>
              <div>
                <p>{WEATHER.avgtempF}</p>
              </div>
            </div>
          ) : (
            <div>{ERROR || "No data found."}</div>
          )}
        </Modal>
      )}
    </>
  );
}

City.propTypes = {
  item: shape({
    id: string,
  }),
  loading: bool,
  deleteWeatherInfo: func,
  getWeather: func,
  data: object,
};

const mapStateToProps = (state) => ({
  data: state.reminder[TARGET.weather]?.data,
  loading: state.reminder[`${TARGET.weather}Loading`],
});

const mapDispatchToProps = (dispatch) => ({
  getWeather: (city, date) => dispatch(reminderActions.getWeather(city, date)),
  deleteWeatherInfo: () => dispatch(reminderActions.deleteWeatherInfo()),
});

export default connect(mapStateToProps, mapDispatchToProps)(City);
