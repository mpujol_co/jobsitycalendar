import { required, length } from "redux-form-validators";

const FormValidation = {
  required: required({ message: "The field is required" }),
  maxLength: (value) =>
    length({
      max: value,
      message: `The field has to be less than ${value} characters`,
    }),
};

export default FormValidation;
