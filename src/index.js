import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./styles/index.css";
import Calendar from "views/Calendar";
import Reminder from "views/Reminder";
import * as serviceWorker from "./serviceWorker";
import store from "./redux";

ReactDOM.render(
  <Provider store={store}>
    <div className="container">
      <Calendar />
      <Reminder />
    </div>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
