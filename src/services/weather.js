import api from "config/api";

export const getWeather = ({ city, date }) =>
  api.get("", {
    key: process.env.REACT_APP_WEATHER_KEY,
    q: city,
    format: "json",
    num_of_days: "1",
    date,
  });
