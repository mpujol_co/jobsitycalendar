import { createTypes } from "redux-recompose";

const completedTypes = ["UPDATE"];

export const actions = createTypes(completedTypes, "@@CALENDAR");

export const actionCreators = {
  update: (target, value) => (dispatch) =>
    dispatch({ type: actions.UPDATE, payload: value, target }),
};
