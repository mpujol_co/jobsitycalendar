import { createReducer, onReadValue } from "redux-recompose";
import { INITIAL_STATE } from "./constants";
import { actions } from "./actions";

const reducerDescription = {
  [actions.UPDATE]: onReadValue(),
};

const reducer = createReducer(INITIAL_STATE, reducerDescription);

export default reducer;
