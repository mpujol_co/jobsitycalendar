import {
  getWeekDays,
  CURRENT_MONTH,
  CURRENT_YEAR,
  CURRENT_DAY,
} from "utils/date";

export const TARGET = {
  year: "year",
  month: "month",
  day: "day",
  weekdays: "weekdays",
};

export const INITIAL_STATE = {
  [TARGET.weekdays]: getWeekDays(),
  [TARGET.year]: CURRENT_YEAR,
  [TARGET.month]: CURRENT_MONTH,
  [TARGET.day]: CURRENT_DAY,
};
