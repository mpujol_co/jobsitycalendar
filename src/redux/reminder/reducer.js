import {
  createReducer,
  completeState,
  onAppend,
  onReadValue,
  onDelete,
  onReplace,
  completeReducer,
} from "redux-recompose";
import { isDaySelected } from "utils/date";
import { INITIAL_STATE, TARGET } from "./constants";
import { actions } from "./actions";

const initialState = completeState(INITIAL_STATE);

const reducerDescription = {
  primaryActions: [actions.GET],
  override: {
    [actions.ADD]: onAppend(),
    [actions.UPDATE]: onReadValue(),
    [actions.DELETE]: onDelete(),
    [actions.REPLACE]: onReplace(),
    [actions.DELETE_ALL]: (state, { payload: { year, month, day } }) => ({
      ...state,
      [TARGET.list]: state.list.filter(
        (value) => !isDaySelected(value.date, `${year}-${month}-${day}`)
      ),
    }),
  },
};

const reducer = createReducer(
  initialState,
  completeReducer(reducerDescription)
);

export default reducer;
