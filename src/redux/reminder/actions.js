import { createTypes, completeTypes } from "redux-recompose";
import { reset } from "redux-form";
import { TARGET } from "./constants";
import { currentTimestamp } from "utils/date";
import { getWeather } from "services/weather";
const completedTypes = completeTypes([
  "ADD",
  "UPDATE",
  "DELETE",
  "REPLACE",
  "GET",
  "DELETE_ALL",
]);

export const actions = createTypes(completedTypes, "@@REMINDER");

export const actionCreators = {
  add: (value, formName) => (dispatch) => {
    const res = {
      id: currentTimestamp(),
      ...value,
    };
    dispatch({
      type: actions.UPDATE,
      payload: true,
      target: `${TARGET.list}Loading`,
    });
    dispatch({ type: actions.ADD, payload: res, target: TARGET.list });
    dispatch(reset(formName));
    dispatch({
      type: actions.UPDATE,
      payload: false,
      target: `${TARGET.list}Loading`,
    });
  },
  delete: (id) => (dispatch) => {
    dispatch({
      type: actions.UPDATE,
      payload: true,
      target: `${TARGET.list}Loading`,
    });
    dispatch({ type: actions.DELETE, payload: id, target: TARGET.list });
    dispatch({
      type: actions.UPDATE,
      payload: false,
      target: `${TARGET.list}Loading`,
    });
  },
  update: (id, values) => (dispatch) => {
    dispatch({
      type: actions.UPDATE,
      payload: true,
      target: `${TARGET.list}Loading`,
    });
    dispatch({
      type: actions.REPLACE,
      target: TARGET.list,
      condition: (element) => element.id === id,
      payload: { id, ...values },
    });
    dispatch({
      type: actions.UPDATE,
      payload: false,
      target: `${TARGET.list}Loading`,
    });
  },
  getWeather: (city, date) => (dispatch) =>
    dispatch({
      type: actions.GET,
      target: TARGET.weather,
      service: getWeather,
      payload: {
        city,
        date,
      },
      failureSelector: ({ data }) => {
        console.log(data);
        return "error";
      },
    }),
  deleteWeatherInfo: () => (dispatch) =>
    dispatch({
      type: actions.UPDATE,
      payload: {},
      target: TARGET.weather,
    }),
  deleteAllReminders: () => (dispatch, getState) =>
    dispatch({
      type: actions.DELETE_ALL,
      payload: getState().calendar,
    }),
};
