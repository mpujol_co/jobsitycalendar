export const TARGET = {
  list: "list",
  weather: "weather",
};

export const INITIAL_STATE = {
  [TARGET.list]: [],
  [TARGET.weather]: {},
};
