import moment from "moment";
import timezone from "moment-timezone";
import "moment/min/locales.min.js";

moment.locale(process.env.REACT_APP_CURRENT_LANGUAGE);

const TIME_ZONE = moment.tz.guess();

const DATE_FORMAT = {
  YEAR: "YYYY",
  MONTH: "MM",
  DAY: "D",
  TIME: "HH:mm",
  DAY_NAME: "dddd",
  MONTH_NAME: "MMMM",
  DATETIME_LOCAL: "YYYY-MM-DDTHH:mm",
};

export const CURRENT_YEAR = timezone.tz(TIME_ZONE).format(DATE_FORMAT.YEAR);
export const CURRENT_MONTH = timezone.tz(TIME_ZONE).format(DATE_FORMAT.MONTH);
export const CURRENT_DAY = timezone.tz(TIME_ZONE).format(DATE_FORMAT.DAY);

export const getWeekDays = () => {
  const result = [];
  let i = 0;
  while (i < 7) {
    result.push(moment().weekday(i).format(DATE_FORMAT.DAY_NAME));
    i++;
  }
  return result;
};

export const daysInMonth = (year, month) =>
  moment(
    `${year}-${month}`,
    `${DATE_FORMAT.YEAR}-${DATE_FORMAT.MONTH}`
  ).daysInMonth();

export const getDay = (date, format = DATE_FORMAT.DAY) =>
  moment(date).format(format);
export const getMonth = (date, format = DATE_FORMAT.MONTH) =>
  moment(date).format(format);
export const getYear = (date, format = DATE_FORMAT.YEAR) =>
  moment(date).format(format);

export const getTime = (date, format = DATE_FORMAT.TIME) =>
  moment(date).format(format);

export const getMonthName = (month) =>
  moment(month, DATE_FORMAT.MONTH).format(DATE_FORMAT.MONTH_NAME);

export const isWeekend = (date) => {
  const WEEKEND_DAYS = [
    moment().isoWeekday(6).format(DATE_FORMAT.DAY_NAME),
    moment().isoWeekday(7).format(DATE_FORMAT.DAY_NAME),
  ];
  return WEEKEND_DAYS.some(
    (day) => day === moment(date).format(DATE_FORMAT.DAY_NAME)
  );
};

export const isDaySelected = (date, current) =>
  moment(date).format(
    `${DATE_FORMAT.YEAR}-${DATE_FORMAT.MONTH}-${DATE_FORMAT.DAY}`
  ) === current;

export const getPrevMonthAndYear = (month, year) => {
  const date = moment(
    `${year}-${month}`,
    `${DATE_FORMAT.YEAR}-${DATE_FORMAT.MONTH}`
  ).subtract(1, "months");
  return {
    year: moment(date).format(DATE_FORMAT.YEAR),
    month: moment(date).format(DATE_FORMAT.MONTH),
  };
};

export const getNextMonthAndYear = (month, year) => {
  const date = moment(
    `${year}-${month}`,
    `${DATE_FORMAT.YEAR}-${DATE_FORMAT.MONTH}`
  ).add(1, "months");
  return {
    year: moment(date).format(DATE_FORMAT.YEAR),
    month: moment(date).format(DATE_FORMAT.MONTH),
  };
};

export const currentTimestamp = (date = undefined) => moment(date).format("x");

export const dateTimeLocal = (date) =>
  moment(
    date,
    `${DATE_FORMAT.YEAR}-${DATE_FORMAT.MONTH}-${DATE_FORMAT.DAY}`
  ).format(DATE_FORMAT.DATETIME_LOCAL);

export const getDate = (date) =>
  moment(date, DATE_FORMAT.DATETIME_LOCAL).format(
    `${DATE_FORMAT.YEAR}-${DATE_FORMAT.MONTH}-${DATE_FORMAT.DAY}`
  );
